//
//  PetViewController+Extension.swift
//  PetForm
//
//  Created by Sagaya Abdulhafeez on 09/01/2019.
//  Copyright © 2019 Cosmo. All rights reserved.
//

import UIKit

extension PetsViewController{
    @objc func showYesNoAction(sender: UITextField){
        view.endEditing(true)
        let rule = viewModel.selectedRule![0]
        let txt = self.stackView.subviews.filter { (v) -> Bool in
            if  v.accessibilityIdentifier == "\(rule.targets![0])-"{
                return true
            }
            return false
        }[0]
        
        let alert = UIAlertController(title: "", message: sender.placeholder ?? "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "No", style: .default , handler:{ (UIAlertAction)in
            sender.text = "No"
            txt.isHidden = true
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
            sender.text = "Yes"
            txt.isHidden = false

        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel))
        self.present(alert, animated: true, completion: {
            
        })
    }
    @objc func showDatePicker(sender: UITextField){
        DatePickerDialog().show(sender.placeholder ?? "", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .dateAndTime) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                sender.text = formatter.string(from: dt)
            }
        }
    }
    func setupStackView(){
        var inputViews = [UIView]()
        let selectedPage = viewModel.pages[viewModel.currentIndex]
        let pageLabel = setupLabel(for: selectedPage.label ?? "")
        pageLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        self.stackView.addArrangedSubview(pageLabel)
        selectedPage.sections?.forEach({ (section) in
            let sectionLabel = setupLabel(for: section.label ?? "")
            self.stackView.addArrangedSubview(sectionLabel)
            inputViews.append(sectionLabel)
            section.elements?.forEach({ (formElements) in
                switch formElements.type{
                case .some("text"):
                    //Textfield
                    let mand = formElements.isMandatory ?? false
                    let textfield = setupTextField(for: formElements.unique_id ?? "", placeholder: formElements.label ?? "", isMadantory: mand)
                    self.stackView.addArrangedSubview(textfield)
                case .some("embeddedphoto"):
                    //Photo
                    let imageView = setupImage(with: URL(string: formElements.file ?? "")!)
                    self.stackView.addArrangedSubview(imageView)
                case .some("yesno"):
                    //yesno
                    let mand = formElements.isMandatory ?? false
                    let textfield = setupTextField(for: formElements.unique_id ?? "", placeholder: formElements.label ?? "", isMadantory: mand)
                    textfield.addTarget(self, action: #selector(showYesNoAction(sender:)), for: .allEvents)
                    viewModel.selectedRule = formElements.rules!
                    self.stackView.addArrangedSubview(textfield)
                case .some("datetime"):
                    let mand = formElements.isMandatory ?? false
                    let textfield = setupTextField(for: formElements.unique_id ?? "", placeholder: formElements.label ?? "", isMadantory: mand)
                    textfield.addTarget(self, action: #selector(showDatePicker(sender:)), for: .allEvents)
                    self.stackView.addArrangedSubview(textfield)
                default:
                    print("")
                }
            })
        })
        let submitButton = setupSubmitButtonl()
        self.stackView.addArrangedSubview(submitButton)
        submitButton.addTarget(self, action: #selector(submiButtonAction), for: .touchUpInside)
    }
    @objc func submiButtonAction(){
        let textFields = self.stackView.subviews.map { (v) -> UITextField? in
            if let txt = v as? UITextField, txt.accessibilityIdentifier?.last == "*"{
                return txt
            }
            return nil
        }
        var isValidated:Bool?
        textFields.forEach { (textfield) in
            if textfield?.text != ""{
                
            }else{
                isValidated = false
                textfield?.shake()
                return;
            }
        }
        if isValidated != false{
            let vc = PetsViewController()
            vc.viewModel.currentIndex =  self.viewModel.currentIndex + 1
            if (self.viewModel.pages.count ) > vc.viewModel.currentIndex {
                self.present(vc, animated: true, completion: nil)
            }else{
                let alert = UIAlertController(title: "Success", message: "Form Submited!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (_) in
                    vc.viewModel.currentIndex = 0
                    self.present(vc, animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

