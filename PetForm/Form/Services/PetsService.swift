//
//  PetsService.swift
//  PetForm
//
//  Created by Sagaya Abdulhafeez on 09/01/2019.
//  Copyright © 2019 Cosmo. All rights reserved.
//

import UIKit

class PetsService {
    class func FetchPets(_ filename:StaticString, errorCallback:@escaping (_ err:String??) -> Void, resultCallback:@escaping (Form) -> Void){
        if let jsonFilePath = Bundle.main.path(forResource: "\(filename)", ofType: "json"){
            do{
                let jsonNsData = try NSData(contentsOfFile: jsonFilePath, options: .alwaysMapped)
                let data = Data(referencing: jsonNsData)
//                print(String(data: data, encoding: .utf8) ?? "")
                let decoder = JSONDecoder()
                if let _response = try? decoder.decode(Form.self, from: data){
                    resultCallback(_response)
                }else{
                    errorCallback("Not parsable")
                }
            }catch{
                errorCallback("Not parsable")
            }
        }else{
            preconditionFailure("Unable to find file")
        }
    }
}
