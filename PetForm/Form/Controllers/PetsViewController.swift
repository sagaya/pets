//
//  ViewController.swift
//  PetForm
//
//  Created by Sagaya Abdulhafeez on 09/01/2019.
//  Copyright © 2019 Cosmo. All rights reserved.
//

import UIKit

class PetsViewController: PetFormViews {
   
    var viewModel = PetsViewModel()
    
    fileprivate func configureViews() {
        view.addSubview(scrollView)
        scrollView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        scrollView.addSubview(formTitle)
        formTitle.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: nil, trailing: scrollView.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: scrollView.frame.width, height: 50))
        scrollView.addSubview(stackView)
        stackView.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: nil, trailing: scrollView.trailingAnchor, padding: .init(top: 80, left: 30, bottom: 0, right: 30))
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = CGSize(width: view.frame.width, height: (view.frame.height * 2))
    }
    fileprivate func handlePets() {
        viewModel.GetPetSuccess = {[weak self] (form) in
            self?.formTitle.text = form.name ?? ""
            assert(form.pages != nil, "No pages")
            form.pages!.map {
                self?.viewModel.pages.append($0)
            }
            self?.setupStackView()
        }
        viewModel.getPets()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureViews()
        handlePets()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewModel.currentIndex + 1 == viewModel.pages.count{
            let buttons = self.stackView.subviews.map { (v) -> UIButton? in
                if let btn = v as? UIButton{
                    return btn
                }
                return nil
            }
            buttons.map {
                if($0 != nil){
                    $0!.setTitle("Submit", for: .normal)
                }
            }
        }
    }
}


class DummyController: PetsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }
}
