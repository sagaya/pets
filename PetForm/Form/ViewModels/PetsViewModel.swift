//
//  PetsViewModel.swift
//  PetForm
//
//  Created by Sagaya Abdulhafeez on 09/01/2019.
//  Copyright © 2019 Cosmo. All rights reserved.
//

import UIKit

enum FormType{
    case embeddedphoto
    case text
    case yesno
}

struct PetsViewModel {
    typealias ErrorHandler = ((String?) -> Void)
    typealias SuccessHandler = ((Form) -> Void)
    var currentIndex = 0
    var GetPetSuccess: SuccessHandler?
    var GetPetError:ErrorHandler?
    var fileName:StaticString = "pet_adoption"
    var pages = [Page]()
    var selectedRule:[Rules]?
    func getPets(){
        PetsService.FetchPets(fileName, errorCallback: { (err) in
            self.GetPetError!(err ?? "")
        }) { (form) in
            self.GetPetSuccess?(form)
        }
    }
}
