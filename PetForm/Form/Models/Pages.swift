//
//  Pages.swift
//  PetForm
//
//  Created by Sagaya Abdulhafeez on 09/01/2019.
//  Copyright © 2019 Cosmo. All rights reserved.
//

import UIKit

struct Form: Codable {
    var name:String?
    var id:String?
    var pages:[Page]?
}
struct Page: Codable {
    var label:String?
    var sections:[Section]?
}
struct Section: Codable {
    var label:String?
    var elements:[FormElements]?
}

struct FormElements: Codable {
    var type: String?{
        didSet{
            assert(type != nil, "Form type was not set")
        }
    }
    var file:String?
    var unique_id:String?
    var isMandatory:Bool?
    var label:String?
    var rules: [Rules]?
}

struct Rules: Codable {
    var condition:String?
    var value:String?
    var action:String?
    var otherwise:String?
    var targets: [String]?
}
