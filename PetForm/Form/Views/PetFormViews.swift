//
//  PetFormViews.swift
//  PetForm
//
//  Created by Sagaya Abdulhafeez on 09/01/2019.
//  Copyright © 2019 Cosmo. All rights reserved.
//
import UIKit

class PetFormViews: UIViewController {
    lazy var scrollView: UIScrollView = {
        let scroll = UIScrollView()
        return scroll
    }()
    lazy var formTitle: UILabel = {
        let lab = UILabel()
        lab.font = UIFont(name: "AvenirNext-Bold", size: 23)
        lab.textColor = .darkGray
        lab.textAlignment = .center
        return lab
    }()
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 3
        return stackView
    }()
    func setupTextField(for identifier:String, placeholder: String, isMadantory:Bool?) -> UITextField{
        let textfield = UITextField()
        textfield.accessibilityIdentifier = "\(identifier)\( isMadantory! ? "*" : "-" )"
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.addleftViewSpace()
        textfield.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.font: UIFont(name: "AvenirNext-Regular", size: 15)])
        textfield.backgroundColor = UIColor(red:0.94, green:0.94, blue:0.94, alpha:1.0)
        textfield.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textfield.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        return textfield
    }
    func setupLabel(for text:String) -> UILabel{
        let lab = UILabel()
        lab.text = text
        lab.textAlignment = .center
        lab.translatesAutoresizingMaskIntoConstraints = false
        lab.heightAnchor.constraint(equalToConstant: 40).isActive = true
        lab.widthAnchor.constraint(equalToConstant: view.frame.width - 80).isActive = true
        return lab
    }
    func setupSubmitButtonl() -> UIButton{
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Next", for: .normal)
        btn.accessibilityIdentifier = "submitBtn"
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.backgroundColor = UIColor(red:0.96, green:0.65, blue:0.14, alpha:1.0)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btn.layer.cornerRadius = 5.0
        btn.layer.masksToBounds = true
        btn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        btn.widthAnchor.constraint(equalToConstant: view.frame.width - 80).isActive = true
        return btn
    }
    func setupImage(with url:URL) -> UIImageView{
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .cyan
        imageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: view.frame.width - 80).isActive = true
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.load(url: url)
        return imageView
    }
    func setupDatepicker() -> UIPickerView{
        let timePicker: UIPickerView = UIPickerView()
        timePicker.frame = CGRect(x: 0, y: 50, width: self.view.frame.width, height: 200)
        timePicker.backgroundColor = .white
        return timePicker
    }
}
